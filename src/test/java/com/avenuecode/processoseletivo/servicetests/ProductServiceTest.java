package com.avenuecode.processoseletivo.servicetests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.BDDMockito.then;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.avenuecode.processoseletivo.model.Product;
import com.avenuecode.processoseletivo.repository.ProductRepository;
import com.avenuecode.processoseletivo.service.ProductService;
import com.avenuecode.processoseletivo.service.ProductServiceImpl;

public class ProductServiceTest {

	ProductRepository productRepo;
	ProductService<Product> productService;

	@Before
	public void setUp() {
		productRepo = Mockito.mock(ProductRepository.class);
		productService = new ProductServiceImpl(productRepo);
	}

	@Test
	public void shouldCreateProductSuccessfully() throws Exception {
		when(productRepo.findOne(eq(1L))).thenReturn(null);
		doAnswer(returnsFirstArg()).when(productRepo).save(any(Product.class));
		Product product = productService.create(new Product(1L, "Passatempo Pacote", "Cookies Package", null, null, null));
		assertNotNull(product);
		assertEquals("Cookies Package", product.getDescription());
	}

	@Test
	public void shouldUpdateProductSuccessfully() throws Exception {
		when(productRepo.findOne(eq(1L))).thenReturn(Mockito.mock(Product.class));
		doAnswer(returnsFirstArg()).when(productRepo).save(any(Product.class));
		Product product = productService.update(buildDefaultProduct(), 1L);
		assertNotNull(product);
		assertNotEquals("Cookies Package", product.getDescription());
	}

	@Test
	public void shouldDeleteProductSuccessfully() throws Exception {
		when(productRepo.findOne(eq(1L))).thenReturn(Mockito.mock(Product.class));
		doAnswer(returnsFirstArg()).when(productRepo).save(any(Product.class));
		productService.delete(1L);
		then(this.productRepo).should().delete(1L);
	}

	private Product buildDefaultProduct() {
		return new Product(1L, "Passatempo Pacote", "Positive False", null, null, null);
	}

}
