package com.avenuecode.processoseletivo.servicetests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.BDDMockito.then;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.avenuecode.processoseletivo.model.Image;
import com.avenuecode.processoseletivo.repository.ImageRepository;
import com.avenuecode.processoseletivo.service.ImageService;
import com.avenuecode.processoseletivo.service.ImageServiceImpl;

public class ImageServiceTest {

	ImageRepository imageRepo;
	ImageService<Image> imageService;

	@Before
	public void setUp() {
		imageRepo = Mockito.mock(ImageRepository.class);
		imageService = new ImageServiceImpl(imageRepo);
	}

	@Test
	public void shouldCreateImageSuccessfully() throws Exception {
		when(imageRepo.findOne(eq(1L))).thenReturn(null);
		doAnswer(returnsFirstArg()).when(imageRepo).save(any(Image.class));
		Image image = imageService.create(new Image(null, "JPEG", null));
		assertNotNull(image);
		assertEquals("JPEG", image.getType());
	}

	@Test
	public void shouldUpdateImageSuccessfully() throws Exception {
		when(imageRepo.findOne(eq(1L))).thenReturn(Mockito.mock(Image.class));
		doAnswer(returnsFirstArg()).when(imageRepo).save(any(Image.class));
		Image image = imageService.update(buildDefaultImage(), 1L);
		assertNotNull(image);
		assertNotEquals("JPEG", image.getType());
	}

	@Test
	public void shouldDeleteImageSuccessfully() throws Exception {
		when(imageRepo.findOne(eq(1L))).thenReturn(Mockito.mock(Image.class));
		doAnswer(returnsFirstArg()).when(imageRepo).save(any(Image.class));
		imageService.delete(1L);
		then(this.imageRepo).should().delete(1L);
	}

	private Image buildDefaultImage() {
		return new Image(1L, "PNG", null);
	}

}
