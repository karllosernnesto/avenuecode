package com.avenuecode.processoseletivo.controllertests;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.avenuecode.processoseletivo.controller.ImageController;
import com.avenuecode.processoseletivo.controller.ProductController;
import com.avenuecode.processoseletivo.model.Image;
import com.avenuecode.processoseletivo.model.Product;
import com.avenuecode.processoseletivo.service.ImageService;
import com.avenuecode.processoseletivo.service.ProductService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

@RunWith(SpringRunner.class)
@WebMvcTest(ProductController.class)
public class ProductControllerTests {

	@Autowired
	MockMvc mockMvc;
	
	@MockBean
	ImageService<Image> imageServiceMock;
	@MockBean
	ProductService<Product> productServiceMock;
	
	@Autowired
    ObjectMapper objectMapper;
	
	@Test
    public void shouldRetrieveProductSuccssFuly() throws Exception {
        mockMvc.perform(
        		get("/product/{productId}", 1L)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
    }
	
	@Test
    public void shouldCreateImageSuccssFuly() throws Exception {
		Product product = new Product(1L, "Passatempo Pacote", "Cookies Package", null, null, null);
		
        given(productServiceMock.create(product)).willReturn(product);
        mockMvc.perform(
        		post("/product")
            .contentType(MediaType.APPLICATION_JSON)
            .content(new Gson().toJson(product)))
        .andExpect(status().isCreated());
    }
	
	@Test
	public void shouldUpdateImageSuccssFuly() throws Exception {
		Product product = new Product(1L, "Passatempo Pacote", "Cookies Package", null, null, null);

		when(productServiceMock.get(1L)).thenReturn(product);
		mockMvc.perform(
				put("/product/{productId}", product.getId())
				.contentType(MediaType.APPLICATION_JSON)
				.content(new Gson().toJson(product)))
		.andExpect(status().isOk());
	}
	
	@Test
	public void shouldDeleteImageSuccssFuly() throws Exception {
		Product product = new Product(1L, "Passatempo Pacote", "Cookies Package", null, null, null);

		when(productServiceMock.get(1L)).thenReturn(product);
		mockMvc.perform(
				delete("/product/{productId}", product.getId())
				.contentType(MediaType.APPLICATION_JSON)
				.content(new Gson().toJson(product)))
		.andExpect(status().isNoContent());
	}

}
