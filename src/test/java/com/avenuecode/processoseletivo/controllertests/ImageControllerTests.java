package com.avenuecode.processoseletivo.controllertests;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.avenuecode.processoseletivo.controller.ImageController;
import com.avenuecode.processoseletivo.model.Image;
import com.avenuecode.processoseletivo.model.Product;
import com.avenuecode.processoseletivo.service.ImageService;
import com.avenuecode.processoseletivo.service.ProductService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

@RunWith(SpringRunner.class)
@WebMvcTest(ImageController.class)
public class ImageControllerTests {

	@Autowired
	MockMvc mockMvc;
	
	@MockBean
	ImageService<Image> imageServiceMock;
	@MockBean
	ProductService<Product> productServiceMock;
	
	@Autowired
    ObjectMapper objectMapper;
	
	@Test
    public void shouldRetrieveImageSuccssFuly() throws Exception {
        given(imageServiceMock.create(new Image(1L, "PNG", null))).willReturn(new Image(1L, "PNG", null));
        mockMvc.perform(
        		get("/image/{imageId}", 1L)
            .contentType(MediaType.APPLICATION_JSON)
            .content(new Gson().toJson(new Image(1L, "PNG", null))))
        .andExpect(status().isOk());
    }
	
	@Test
    public void shouldCreateImageSuccssFuly() throws Exception {
        given(imageServiceMock.create(new Image(1L, "PNG", null))).willReturn(new Image(1L, "PNG", null));
        mockMvc.perform(
        		post("/image")
            .contentType(MediaType.APPLICATION_JSON)
            .content(new Gson().toJson(new Image(1L, "PNG", null))))
        .andExpect(status().isCreated());
    }
	
	@Test
	public void shouldUpdateImageSuccssFuly() throws Exception {
		Product parent = new Product(1L, "Passatempo Pacote", "Cookies Package", null, null, null);
		Image image = new Image(1L, "jpeg", parent);

		when(imageServiceMock.get(1L)).thenReturn(image);
		mockMvc.perform(
				put("/image/{imageId}", image.getId())
				.contentType(MediaType.APPLICATION_JSON)
				.content(new Gson().toJson(image)))
		.andExpect(status().isOk());
	}
	
	@Test
	public void shouldDeleteImageSuccssFuly() throws Exception {
		Product parent = new Product(1L, "Passatempo Pacote", "Cookies Package", null, null, null);
		Image image = new Image(1L, "jpeg", parent);

		when(imageServiceMock.get(1L)).thenReturn(image);
		mockMvc.perform(
				delete("/image/{imageId}", image.getId())
				.contentType(MediaType.APPLICATION_JSON)
				.content(new Gson().toJson(image)))
		.andExpect(status().isGone());
	}

}
