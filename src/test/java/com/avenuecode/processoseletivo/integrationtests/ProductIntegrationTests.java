package com.avenuecode.processoseletivo.integrationtests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.avenuecode.processoseletivo.model.Image;
import com.avenuecode.processoseletivo.model.Product;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class ProductIntegrationTests {
    @Autowired
    private TestRestTemplate restTemplate;
    
    private static String URL = "/product";
    private static String URL_PARAM_ID = URL.concat("/{productId}");
    
    @Test
	public void createProduct() {
		ResponseEntity<Product> responseEntity = restTemplate.postForEntity(URL,
				new Product(1L, "Passatempo Pacote", "Cookies Package", null, null, null), Product.class);
		Product product = responseEntity.getBody();
		assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
		assertEquals("Cookies Package", product.getDescription());
	}
    
    @Test
	public void updateProduct() {
		Product product = new Product(1L, "Passatempo Pacote", "Cookies Package", null, null, null);
		HttpEntity<Product> requestEntity = new HttpEntity<Product>(product);

		ResponseEntity<Void> responseEntity = restTemplate.exchange(URL_PARAM_ID, HttpMethod.PUT, requestEntity, Void.class, 1L	);

		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	}
    
    public void deleteProduct() throws Exception {
    	ResponseEntity<Void> responseEntity = restTemplate.exchange(URL_PARAM_ID, HttpMethod.DELETE, null, Void.class, 1L);

    	assertEquals(HttpStatus.GONE, responseEntity.getStatusCode());

    }
    
	public void getProduct() throws Exception {

		ResponseEntity<Product> responseEntity = restTemplate.getForEntity(URL_PARAM_ID, Product.class, 1L);

		Product resultEmployee = responseEntity.getBody();

		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

		assertNotNull(resultEmployee);
		assertEquals(1l, resultEmployee.getId().longValue());

	}
}