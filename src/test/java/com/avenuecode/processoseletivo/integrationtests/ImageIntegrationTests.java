package com.avenuecode.processoseletivo.integrationtests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.avenuecode.processoseletivo.model.Image;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class ImageIntegrationTests {
    @Autowired
    private TestRestTemplate restTemplate;
    
    private static String URL = "/image";
    private static String URL_PARAM_ID = URL.concat("/{imageId}");
    
    @Test
	public void createImage() {
		ResponseEntity<Image> responseEntity = restTemplate.postForEntity(URL, new Image(1L, "jpeg", null),
				Image.class);
		Image image = responseEntity.getBody();
		assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
		assertEquals("jpeg", image.getType());
	}
    
    @Test
	public void updateImage() {
		Image image = new Image(1L, "jpeg", null);
		HttpEntity<Image> requestEntity = new HttpEntity<Image>(image);

		ResponseEntity<Void> responseEntity = restTemplate.exchange(URL_PARAM_ID, HttpMethod.PUT, requestEntity, Void.class, 1L	);

		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	}
    
    public void deleteImage() throws Exception {
    	ResponseEntity<Void> responseEntity = restTemplate.exchange(URL_PARAM_ID, HttpMethod.DELETE, null, Void.class, 1L);

    	assertEquals(HttpStatus.GONE, responseEntity.getStatusCode());

    }
    
	public void getImage() throws Exception {

		ResponseEntity<Image> responseEntity = restTemplate.getForEntity(URL_PARAM_ID, Image.class, 1L);

		Image resultEmployee = responseEntity.getBody();

		assertEquals(HttpStatus.OK,  responseEntity.getStatusCode());

		assertNotNull(resultEmployee);
		assertEquals(1l, resultEmployee.getId().longValue());

	}
}