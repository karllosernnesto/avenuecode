package com.avenuecode.processoseletivo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.avenuecode.processoseletivo.model.Product;
import com.avenuecode.processoseletivo.repository.ProductRepository;

/**
 * @author Karllos Ernnesto
 *
 */
@Service
public class ProductServiceImpl implements ProductService<Product> {

	@Autowired ProductRepository productRepo;

	/* (non-Javadoc)
	 * @see com.avenuecode.processoseletivo.service.BaseService#create(java.lang.Object)
	 */
	@Override
	public Product create(Product product) {
		return productRepo.save(product);
	}
	
	/* (non-Javadoc)
	 * @see com.avenuecode.processoseletivo.service.BaseService#createAll(java.util.List)
	 */
	@Override
	public List<Product> createAll(List<Product> entity) {
		return productRepo.save(entity);
	}

	/* (non-Javadoc)
	 * @see com.avenuecode.processoseletivo.service.BaseService#get(java.lang.Object)
	 */
	@Override
	public Product get(Object productId) {
		return productRepo.findOne((Long)productId);
	}

	/* (non-Javadoc)
	 * @see com.avenuecode.processoseletivo.service.BaseService#update(java.lang.Object, java.lang.Object)
	 */
	@Override
	public Product update(Product product, Object entityId) {
		return productRepo.save(product);
	}

	/* (non-Javadoc)
	 * @see com.avenuecode.processoseletivo.service.BaseService#delete(java.lang.Object)
	 */
	@Override
	public void delete(Object productId) {
		productRepo.delete((Long)productId);
	}

	/* (non-Javadoc)
	 * @see com.avenuecode.processoseletivo.service.BaseService#list()
	 */
	@Override
	public List<Product> list() {
		return productRepo.findAll();
	}
	
	@Override
	public List<Product> findChildProductsById(Long id) {
		return productRepo.findChildProductsById(id);
	}
	
	/**
	 * @param productRepo
	 */
	public ProductServiceImpl(ProductRepository productRepo) {
		this.productRepo = productRepo;
	}


}
