package com.avenuecode.processoseletivo.service;

import java.util.List;

public interface BaseService<T> {

	public T create(T entity);
	public List<T> createAll(List<T> entity);
	public T get(Object entityId);
	public T update(T entity, Object entityId);
	public void delete(Object entityId);
	public List<T> list();
}
