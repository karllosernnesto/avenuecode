package com.avenuecode.processoseletivo.service;

import java.util.List;

public interface ImageService<Image> extends BaseService<Image> {
	
	List<Image> findImagesByProduct(Long productId);
	

}
