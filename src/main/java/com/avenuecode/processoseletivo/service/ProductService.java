package com.avenuecode.processoseletivo.service;

import java.util.List;


public interface ProductService<Product> extends BaseService<Product> {
	
	List<Product> findChildProductsById(Long id);
	
}
