package com.avenuecode.processoseletivo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.avenuecode.processoseletivo.model.Image;
import com.avenuecode.processoseletivo.repository.ImageRepository;

/**
 * @author Karllos Ernnesto
 *
 */
@Service
public class ImageServiceImpl implements ImageService<Image> {

	@Autowired final ImageRepository imageRepo;

	/* (non-Javadoc)
	 * @see com.avenuecode.processoseletivo.service.BaseService#create(java.lang.Object)
	 */
	@Override
	public Image create(Image image) {
		return imageRepo.save(image);
	}
	
	/* (non-Javadoc)
	 * @see com.avenuecode.processoseletivo.service.BaseService#createAll(java.util.List)
	 */
	@Override
	public List<Image> createAll(List<Image> entity) {
		return imageRepo.save(entity);
	}

	/* (non-Javadoc)
	 * @see com.avenuecode.processoseletivo.service.BaseService#get(java.lang.Object)
	 */
	@Override
	public Image get(Object imageId) {
		return imageRepo.findOne((Long)imageId);
	}

	/* (non-Javadoc)
	 * @see com.avenuecode.processoseletivo.service.BaseService#update(java.lang.Object, java.lang.Object)
	 */
	@Override
	public Image update(Image image, Object entityId) {
		return imageRepo.save(image);
	}

	/* (non-Javadoc)
	 * @see com.avenuecode.processoseletivo.service.BaseService#delete(java.lang.Object)
	 */
	@Override
	public void delete(Object imageId) {
		imageRepo.delete((Long)imageId);
	}

	/* (non-Javadoc)
	 * @see com.avenuecode.processoseletivo.service.BaseService#list()
	 */
	@Override
	public List<Image> list() {
		return imageRepo.findAll();
	}

	/* (non-Javadoc)
	 * @see com.avenuecode.processoseletivo.service.ImageService#findImagesByProduct(java.lang.Long)
	 */
	@Override
	public List<Image> findImagesByProduct(Long productId) {
		return imageRepo.findImagesByProduct(productId);
	}

	/**
	 * @param imageRepo
	 */
	public ImageServiceImpl(ImageRepository imageRepo) {
		this.imageRepo = imageRepo;
	}


}
