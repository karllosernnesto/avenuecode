/**
 * 
 */
package com.avenuecode.processoseletivo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.avenuecode.processoseletivo.model.Product;

/**
 * @author Karllos Ernnesto
 *
 */
@RepositoryRestResource(collectionResourceRel = "testeProduct", path = "testeProduct")
public interface ProductRepository extends JpaRepository<Product, Long>{
	
	@Query("select p from Product p where p.parentProduct.id = :id")
	List<Product> findChildProductsById(@Param(value = "id") Long id);
	
}
