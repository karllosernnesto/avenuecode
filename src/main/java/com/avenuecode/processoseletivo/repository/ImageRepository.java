package com.avenuecode.processoseletivo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.avenuecode.processoseletivo.model.Image;

@RepositoryRestResource(collectionResourceRel = "testeImage", path = "testeImage")
public interface ImageRepository extends JpaRepository<Image, Long> {

	@Query("SELECT i FROM Image i where i.product.id = :productId") 
	List<Image> findImagesByProduct(@Param("productId") Long productId);
}
