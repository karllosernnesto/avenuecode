package com.avenuecode.processoseletivo.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * @author Karllos Ernnesto
 *
 */
@Entity
public class Product implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id @GeneratedValue
	private Long id;
	private String name;
	private String description;
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonBackReference("productParent")
	private Product parentProduct;
	@OneToMany(mappedBy="parentProduct", fetch = FetchType.LAZY)
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@JsonManagedReference("productParent")
	private Set<Product> childProduct;
	@OneToMany(mappedBy="product", fetch = FetchType.LAZY)
	@JsonManagedReference @JsonInclude(JsonInclude.Include.NON_EMPTY)
	private Set<Image> images;

	/**
	 * 
	 */
	public Product() {
	}

	/**
	 * @param id
	 * @param name
	 * @param description
	 * @param parentProduct
	 * @param childProduct
	 * @param images
	 */
	public Product(Long id, String name, String description, Product parentProduct, Set<Product> childProduct,
			Set<Image> images) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.parentProduct = parentProduct;
		this.childProduct = childProduct;
		this.images = images;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the parentProduct
	 */
	public Product getParentProduct() {
		return parentProduct;
	}

	/**
	 * @param parentProduct the parentProduct to set
	 */
	public void setParentProduct(Product parentProduct) {
		this.parentProduct = parentProduct;
	}

	/**
	 * @return the childProduct
	 */
	public Set<Product> getChildProduct() {
		return childProduct;
	}

	/**
	 * @param childProduct the childProduct to set
	 */
	public void setChildProduct(Set<Product> childProduct) {
		this.childProduct = childProduct;
	}

	/**
	 * @return the images
	 */
	public Set<Image> getImages() {
		return images;
	}

	/**
	 * @param images the images to set
	 */
	public void setImages(Set<Image> images) {
		this.images = images;
	}

}
