package com.avenuecode.processoseletivo.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author Karllos Ernnesto
 *
 */
@Entity
public class Image implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Long id;
	private String type;
	@ManyToOne
	@JsonBackReference
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private Product product;

	/**
	 * 
	 */
	public Image() {
	}

	/**
	 * @param id
	 * @param type
	 * @param product
	 */
	public Image(Long id, String type, Product product) {
		this.id = id;
		this.type = type;
		this.product = product;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}

	/**
	 * @param product the product to set
	 */
	public void setProduct(Product product) {
		this.product = product;
	}



}
