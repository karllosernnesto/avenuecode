package com.avenuecode.processoseletivo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.avenuecode.processoseletivo.model.Image;
import com.avenuecode.processoseletivo.model.Product;
import com.avenuecode.processoseletivo.service.ImageService;
import com.avenuecode.processoseletivo.service.ProductService;

@SpringBootApplication
public class AvenueCodeApplication {

	@Autowired ProductService<Product> productService;
	@Autowired ImageService<Image> imageService;

	public static void main(String[] args) {
		SpringApplication.run(AvenueCodeApplication.class, args);
	}

	@PostConstruct
	public void initProducts() {

		Product parent = new Product(1L, "Passatempo Pacote", "Cookies Package", null, null, null);
		productService.create(parent);
		Product child = new Product(2L, "Passatempo", "Cookies", parent, null, null);
		productService.create(child);
		List<Image> h = new ArrayList<Image>(Arrays.asList(new Image(1L, "jpeg", parent), new Image(2L, "png", parent)));
		imageService.createAll(h);
	}
}
