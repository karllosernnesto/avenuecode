package com.avenuecode.processoseletivo.controller;

import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.avenuecode.processoseletivo.model.Image;
import com.avenuecode.processoseletivo.model.Product;
import com.avenuecode.processoseletivo.service.ImageService;
import com.avenuecode.processoseletivo.service.ProductService;

@RestController
public class ProductController {

	@Autowired ProductService<Product> productService;
	@Autowired ImageService<Image> imageService;

	@GetMapping("/products")
	public List<Product> findAll() {
		List<Product> products = productService.list();
		// Remove os parents de product, mesmo que ele ja esteja na sessao do jpa. Caso
		// contrario o json serializa pois o objeto ja se encontra na sessao
		products.forEach(product -> product.setParentProduct(null));
		return products;
	}
	
	@GetMapping("/products/withRel")
	public List<Product> findAllWithRel(){
		List<Product> products = productService.list();
		for (Product product : products) {
			Hibernate.initialize(product.getChildProduct());
			Hibernate.initialize(product.getImages());
		}
		//		products.forEach(product -> product.setImages(new HashSet<Image>(imageService.findImagesByProduct(product.getId()))));
		return products;
	}
	
	@GetMapping("/products/withRel/{productId}")
	public Product findAllWithRel(@PathVariable Long productId){
		Product product = productService.get(productId);
		Hibernate.initialize(product.getChildProduct());
		Hibernate.initialize(product.getImages());
		return product;
	}
	
	@GetMapping("/product/{productId}/childs")
	public List<Product> findChildsByParent(@PathVariable Long productId){
		return  productService.findChildProductsById(productId);
	}
	
	@GetMapping("/product/{productId}/images")
	public List<Image> findImagesByProduct(@PathVariable Long productId){
		return  imageService.findImagesByProduct(productId);
	}

	@PostMapping("/product")
	@ResponseStatus(HttpStatus.CREATED)
	public Product create (@RequestBody Product product){
		return productService.create(product);
	}

	@GetMapping("/product/{productId}")
	public Product get (@PathVariable Long productId){
		return productService.get(productId);
	}

	@PutMapping("/product/{productId}")
	public void update (@RequestBody Product product, @PathVariable Long productId){
		productService.update(product, productId);
	}

	@DeleteMapping("/product/{productId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete (@PathVariable Long productId){
		productService.delete(productId);
	}
	
	
}
