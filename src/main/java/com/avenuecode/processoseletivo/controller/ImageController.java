package com.avenuecode.processoseletivo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.avenuecode.processoseletivo.model.Image;
import com.avenuecode.processoseletivo.service.ImageService;

@RestController
public class ImageController {

	@Autowired ImageService<Image> imageService;

	@GetMapping("/images")
	public List<Image> findAll(){
		return imageService.list();
	}

	@PostMapping("/image")
	@ResponseStatus(HttpStatus.CREATED)
	public Image create (@RequestBody Image image){
		return imageService.create(image);
	}

	@GetMapping("/image/{imageId}")
	public Image get (@PathVariable Long imageId){
		return imageService.get(imageId);
	}

	@PutMapping("/image/{imageId}")
	public void update (@RequestBody Image image, @PathVariable Long imageId){
		imageService.update(image, imageId);
	}

	@DeleteMapping("/image/{imageId}")
	@ResponseStatus(HttpStatus.GONE)
	public void delete (@PathVariable Long imageId){
		imageService.delete(imageId);
	}
}
