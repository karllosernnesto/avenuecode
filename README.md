# README #

This README would normally document whatever steps are necessary to get your application up and running.

## Running Application

###Running as a packaged application
```bash
	mvn package
	java -jar target/AvenueCode-0.0.1-SNAPSHOT.jar
```

###Using the Maven plugin
```bash
	mvn spring-boot:run
```

## How to use the Application

* ###cURL
	obs.: Use \ to escape " in Win OS

####CREATE
```bash
	curl -X POST -H "Content-Type: application/json" -d "{ \"type\": \"TIFF\" }" localhost:8080/image
	curl -X POST -H "Content-Type: application/json" -d "{ \"type\" : \"GIF\",\"product\" : {\"id\" : 1 }}" localhost:8080/image

	curl -X POST -H "Content-Type: application/json" -d "{ \"name\" : \"Biscoito\", \"description\" : \"Um unico biscoito\", \"parentProduct\" : { \"id\" : 2 }}" localhost:8080/product
```

####REQUEST
```bash
	curl -X GET -H "Content-Type: application/json" localhost:8080/image/3

	curl -X GET -H "Content-Type: application/json" localhost:8080/product/3
```

####UPDATE
```bash
	curl -X PUT -H "Content-Type: application/json" -d "{ \"id\": 3,\"type\": \"BMP\" }" localhost:8080/image/3
	curl -X PUT -H "Content-Type: application/json" -d "{ \"id\" : \"4\",\"type\" : \"BPG\",\"product\" : {\"id\" : 2} }" localhost:8080/image/3
	
	curl -X PUT -H "Content-Type: application/json" -d "{ \"id\" :3, \"name\" : \"Biscoito\", \"description\" : \"Um unico biscoito\", \"parentProduct\" : { \"id\" : 1}}" localhost:8080/product/3
```

####DELETE
```bash
	curl -X DELETE -H "Content-Type: application/json" localhost:8080/image/4
	
	curl -X DELETE -H "Content-Type: application/json" localhost:8080/product/3
```

* ###Postman Chrome Extension

####CREATE
	localhost:8080/image
	Method.POST
	{ "type": "TIFF" }
	
	localhost:8080/image
	Method.POST
	{ "id" : "4","type" : "BPG","product" : {"id" : 2} }
	
	localhost:8080/product
	Method.POST
	{ "id" :3,	"name" : "Biscoito", "description" : "Um unico biscoito", "parentProduct" : { "id" : 1}}

####REQUEST
	localhost:8080/image/3
	Method.GET
	
	localhost:8080/product/3
	Method.GET

####UPDATE
	localhost:8080/image/3
	Method.PUT
	{ "id": 3,"type": "BMP" }
	
	localhost:8080/product/3
	Method.PUT
	{ "id" :3,	"name" : "Biscoito", "description" : "Um unico biscoito", "parentProduct" : { "id" : 1}}

####DELETE
	localhost:8080/image/4
	Method.DELETE
	
	localhost:8080/product/3
	Method.DELETE
	
## Get all products excluding relationships
	localhost:8080/products/

## Get all products including specified relationships
	localhost:8080/products/withRel/

## Get all products excluding relationships specifying product identity 
	localhost:8080/product/2

## Get all products including specified relationships specifying product identity 
	localhost:8080/products/withRel/1

## Get set of child products for specific product 
	localhost:8080/product/1/childs

## Get set of images for specific product
	localhost:8080/product/1/images